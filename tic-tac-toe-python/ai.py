#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 16 2018

@author: Michael Valdron
"""

import numpy as np
from ortools.constraint_solver import pywrapcp


class AI:
    """docstring forAI."""

    def __init__(self, name="tictactoe_solver"):
        self.dim = 3
        self.entries = np.empty(shape=[self.dim, self.dim], dtype=object)
        self.solver = pywrapcp.Solver(name)
        self.coords = dict()

    def load(self, data):
        assert type(data) == type(list()) \
               and len(data) == self.dim \
               and len(data[0]) == self.dim
        for rid in range(len(data)):
            for cid in range(len(data[rid])):
                label = "var%d_%d" % (rid, cid)
                self.coords[label] = (rid, cid)
                if data[rid][cid]['isx']:
                    self.entries[rid, cid] = self.solver.IntConst(1, label)
                elif data[rid][cid]['iso']:
                    self.entries[rid, cid] = self.solver.IntConst(0, label)
                else:
                    self.entries[rid, cid] = self.solver.BoolVar(label)

    def make_constraints(self):
        # Add Row and Columns Constraints
        for i in range(self.dim):
            for s in range(0, 2):
                self.solver.Add(self.solver.Count(list(self.entries[i, :]), s, self.solver.IntVar(1, 2)))
                self.solver.Add(self.solver.Count(list(self.entries[:, i]), s, self.solver.IntVar(1, 2)))
        # Add Diagonal Constraints
        for s in range(0, 2):
            self.solver.Add(self.solver.Count(list(self.entries.diagonal()), s, self.solver.IntVar(1, 2)))
            self.solver.Add(self.solver.Count(list(np.fliplr(self.entries).diagonal()), s, self.solver.IntVar(1, 2)))

    def solve(self):
        sol = []
        db = self.solver.Phase(list(self.entries.reshape(self.dim * self.dim)), self.solver.CHOOSE_FIRST_UNBOUND,
                               self.solver.ASSIGN_MIN_VALUE)

        self.solver.NewSearch(db)

        if self.solver.NextSolution():
            sol = [v.Value() for v in self.entries.reshape(self.dim * self.dim)]

        self.solver.EndSearch()

        return sol
