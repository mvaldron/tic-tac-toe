#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 15 2018

@author: Michael Valdron
"""

import sys
import os
import pygame
import ai

X = "X"
O = "O"

grid = [[{} for _ in range(3)] for _ in range(3)]


class GamePiece:
    def __init__(self):
        self.size = None
        self.ptype = None
        self.pos = None
        self.surf = None

    def update(self, dis):
        dis.blit(self.surf, (self.pos[0] - (self.size[0] / 2), self.pos[1] - (self.size[1] / 2)))
        pygame.display.update()

    def place_piece(self, dis, ptype, pos):
        self.ptype = ptype
        self.pos = pos
        self.size = (int(dis.get_width() / 8), int(dis.get_height() / 8))
        self.draw_piece(dis)

    def draw_piece(self, dis):
        self.surf = pygame.image.load(os.path.join('img', get_image(self.ptype)))
        self.surf = pygame.transform.scale(self.surf, self.size)
        self.update(dis)

    def remove(self, dis):
        self.surf.fill([255, 255, 255])
        self.update(dis)


def get_image(ptype):
    return {
        X: 'x.png',
        O: 'o.png'
    }[ptype]


def preset_board(dis, filepath):
    ######## Temp Fixed Board Preset: This will be changed to dynamic preset soon. ########
    for rid in range(len(grid)):
        for cid in range(len(grid[rid])):
            if rid == 0:
                if cid == 1:
                    grid[rid][cid]['isempty'] = False
                    grid[rid][cid]['iso'] = True
                    grid[rid][cid]['sprite'] = GamePiece()
                    grid[rid][cid]['sprite'].place_piece(dis, O, grid[rid][cid]['cpos'])
            elif rid == 1:
                if cid == 0 or cid == 1:
                    grid[rid][cid]['isempty'] = False
                    grid[rid][cid]['isx'] = True
                    grid[rid][cid]['sprite'] = GamePiece()
                    grid[rid][cid]['sprite'].place_piece(dis, X, grid[rid][cid]['cpos'])
    #######################################################################################


def draw_grid(dis):
    x = 0
    y = 0
    dx = dis.get_width() / 4
    dy = dis.get_height() / 4
    xmargin = dis.get_width() / 8
    ymargin = dis.get_height() / 8
    for rid in range(3):
        x = 0
        for cid in range(3):
            if rid < 2:
                centerx = pygame.draw.line(dis, (0, 0, 0), (x + xmargin, (y + dy) + ymargin),
                                           ((x + dx) + xmargin, (y + dy) + ymargin), (1)).centerx
            else:
                centerx = int(((x + xmargin) + ((x + dx) + xmargin)) / 2)

            if cid < 2:
                centery = pygame.draw.line(dis, (0, 0, 0), ((x + dx) + xmargin, y + ymargin),
                                           ((x + dx) + xmargin, (y + dy) + ymargin), (1)).centery
            else:
                centery = int(((y + ymargin) + ((y + dy) + ymargin)) / 2)

            grid[rid][cid]['cpos'] = (centerx, centery)
            grid[rid][cid]['isempty'] = True
            grid[rid][cid]['iso'] = False
            grid[rid][cid]['isx'] = False
            grid[rid][cid]['sprite'] = None

            x += dx
        y += dy
    pygame.display.update()


def draw_bg(dis):
    dis.fill([255, 255, 255])
    draw_grid(dis)
    return dis


def init():
    pygame.init()
    pygame.display.set_caption('Tic Tac Toe')
    return draw_bg(pygame.display.set_mode([600, 600]))


def run(dis, filepath, ai_side, dtime):
    preset_board(dis, filepath)
    pressed = False
    while True:
        # 30 fps
        pygame.time.Clock().tick(dtime)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit(0)
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_q:
                sys.exit(0)
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_RETURN:
                if not pressed:
                    pressed = True
                    s = ai.AI()
                    s.load(grid)
                    s.make_constraints()
                    sol = ai.np.asarray(s.solve()).reshape(3, 3)
                    print(sol)
                    for rid in range(len(sol[:, 0])):
                        for cid in range(len(sol[rid, :])):
                            if sol[rid, cid] == 0:
                                grid[rid][cid]['isempty'] = False
                                grid[rid][cid]['iso'] = True
                                grid[rid][cid]['sprite'] = GamePiece()
                                grid[rid][cid]['sprite'].place_piece(dis, O, grid[rid][cid]['cpos'])
                            else:
                                grid[rid][cid]['isempty'] = False
                                grid[rid][cid]['isx'] = True
                                grid[rid][cid]['sprite'] = GamePiece()
                                grid[rid][cid]['sprite'].place_piece(dis, X, grid[rid][cid]['cpos'])

            # elif event.type == pygame.MOUSEBUTTONDOWN and event:
