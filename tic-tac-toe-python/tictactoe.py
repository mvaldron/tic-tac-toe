#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 16 2018

@author: Michael Valdron
"""

import sys
import argparse
from sim import run, init

def main():
    parser = argparse.ArgumentParser(description='Tic Tac Toe Simulation of problem which is solved by AI.')
    parser.add_argument('-f', type=str, dest='filepath', help='Path to file with presetted board.')
    parser.add_argument('-s', type=str, dest='side',
                        help='Side of AI (X or O).', default='O',
                        choices=['X', 'O'])
    args = parser.parse_args()
    run(init(), args.filepath, args.side, 30)
    return 0

if __name__ == '__main__':
    sys.exit(main())
