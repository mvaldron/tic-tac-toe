(ns tic-tac-toe.core-test
  (:require [tic-tac-toe.dbg :refer :all]
            [clojure.test :refer :all]
            [schema.core :as s]
            [tic-tac-toe.sandbox :refer :all]))

(s/set-fn-validation! true)

(defn wrap-test [f]
  (init-solver!)
  (f))

(use-fixtures :once wrap-test)

; (deftest make-solver-test
;   (testing "Allocating solver.."
;     (is (not (nil? @solver)))))

; (deftest make-empty-vars-test
;   (testing "Allocating vars.."
;     (is
;       (let [x (make-empty-var)]
;         (= 9 (count (:board x)))))))

; (deftest add-constraint-test
;   (testing "Adding Constraint"
;     (let [[x y] (make-int-vars 2)]
;       (do
;         (add-constraints (eq- x (piece :x))
;                          (neq- y (piece :o)))
;         (is (<= 2 (count-constraints)))))))

; (deftest eq-val-test
;   (testing "Eq-Val"
;     (let [x (make-empty-var)
;           v (make-state-val [:x :o :x 
;                              :b :b :o 
;                              :b :x :b] :o)
;           s (Solve1 {:state-vars [x] :constraints (Set x v)})]
;       (println "Eq-Val:")
;       (print-state-val (first s))
;       (is (= (first s) v)))))

; (deftest or-test
;   (testing "or"
;     (let [x (first (make-int-vars 1))
;           c1 (eq- x (piece :o))
;           c2 (eq- x (piece :b))
;           c (or- c1 c2)
;           s (solve1 {:int-vars [x] :constraints [c]})]
;       (println "OR Test: " s)
;       (is true))))

; (deftest and-test
;   (testing "and"
;     (let [[x y] (make-int-vars 2)
;           s (solve1 {:int-vars [x y] 
;                      :constraints [(and- (eq- x (piece :b)) (eq- x (piece :x)))]})]
;       (println "And Test:" (map piece-of s)))))

; (deftest imply-test
;   (testing "imply"
;     (let [[x y] (make-int-vars 2)
;           s (solve1 {:int-vars [y]
;                      :constraints [(eq- x (piece :o))
;                                    (imply- (neq- x (piece :b)) (eq- x y))]})]
;       (println "Imply Test: " (map piece-of s)))))

; (deftest unique-test
;   (testing "unique"
;     (let [xs (make-int-vars 9)
;           c1 (uniq- (for [x xs]
;                       (eq- x (piece :x))))
;           s (solve1 {:int-vars xs
;                      :constraints [c1]})]
;       (println "Unique Test:" (map piece-of s)))))

; (deftest move-test
;   (testing "move"
;     (let [x (make-empty-var)
;           y (make-empty-var)
;           z (make-empty-var)
;           c1 (Set x {:board [:x :o :b 
;                              :b :o :b
;                              :x :b :x]
;                      :next :o})
;           c2 (Move x y)
;           c3 (Move y z)
;           [sx sy sz] (Solve1 {:state-vars [x y z]
;                      :constraints (concat c1 c2 c3)})]
;       (println "Move Test:")
;       (print-state-val sx)
;       (print-state-val sy)
;       (print-state-val sz)
;       (is true))))

(deftest safe-test
  (testing "safe"
    (let [x (make-empty-var)
          y (make-empty-var)
          yp (make-empty-var)
          z (make-empty-var)
          zp (make-empty-var)
          c1 (Set x {:board [:b :b :x 
                             :o :x :b
                             :b :b :b]
                      :next :o})
          c2 (Move x y)
          c3 (Safe y :o :x)
          c4 (Win y yp :o :x)
          c5 (Move y z)
          c6 (Safe z :x :o)
          c7 (Win z zp :x :o)
          [sx sy syp sz szp] (Solve1 {:state-vars [x y yp z zp]
                                      :constraints (concat c1 c2 c3 c4 c5 c6 c7)})]
      (println "Safe Test:")
      (print-state-val sx)
      (println "Move 1:")
      (print-state-val sy)
      (println "Win Solution from y:")
      (print-state-val syp)
      (println "Move 2:")
      (print-state-val sz)
      (println "Win Solution from z:")
      (print-state-val szp)
      (is true))))

; (deftest unwinnable-test
;   (testing "Unwinnable"
;     (let [x (make-empty-var)
;           y (make-empty-var)
;           c1 (Set x {:board [:o :o :x 
;                              :x :b :o
;                              :x :x :b]
;                      :next :o})
;           c2 (Move x y)
;           c3 (Safe y :o :x)
;           c4 (Winning y :o :x)
;           c5 (Win y :o :x)
;           [sx sy] (Solve1 {:state-vars [x y]
;                            :constraints (concat c1 c2 c3 c4 c5)})]
;       (println "Unwinnable Test:")
;       (println sx)
;       (println sy)
;       (is (and (nil? sx) (nil? sy))))))

; (deftest gameplay-test
;   (testing "gameplay"
;     (let [solver (fn [state-val ai-side player-side]
;                     (let [x (make-empty-var)
;                           y (make-empty-var)
;                           yp (make-empty-var)
;                           c1 (Set x state-val)
;                           c2 (Move x y)
;                           c3 (Safe y ai-side player-side)
;                           c4 (Win y yp ai-side player-side)
;                           ;c4 (Winning y ai-side player-side)
;                           ;c5 (Win y ai-side player-side)
;                           [sx sy syp] (Solve1 {:state-vars [x y yp]
;                                            :constraints (concat c1 c2 c3 c4)})] ;c4 c5)})]
;                       (println sy)
;                       sy))
;           player :x
;           ai :o]
;       (loop [pre-state nil
;              curr-state {:board [:b :b :b 
;                                  :b :b :b
;                                  :b :b :b]
;                          :next :x}]
;         (if (nil? curr-state) 
;           pre-state
;           (do
;             (print-state-val curr-state)
;             (if (= (:next curr-state) player)
;               (let [ppos (as-> (do (print "Choose piece position (1-9): ") (flush) (read-line)) $
;                             (Integer/parseInt $)
;                             (- $ 1))]
;                 (recur curr-state (update (assoc curr-state :next ai) :board #(assoc % ppos player))))
;               (recur curr-state (solver curr-state ai player))))))
;       (is true))))
