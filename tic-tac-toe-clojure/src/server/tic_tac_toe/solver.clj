(ns tic-tac-toe.solver
  (:gen-class)
  (:require [tic-tac-toe.state :refer :all]
            [schema.core :as s]
            [clojure.string :as strs])
  (:import
    (com.google.protobuf GeneratedMessageV3)
    (com.google.ortools.constraintsolver DecisionBuilder
                                         Constraint
                                         IntVar
                                         Solver)))

(def solver (atom nil))

(s/defn make-solver
  ([] (make-solver "tictactoe_solver"))
  ([name :- s/Str] (Solver. name)))
                                        
(defn make-int-vars [n l u]
  (vec (.makeIntVarArray @solver n l u)))
                                        
(defn make-bool-vars [n]
  (vec (.makeIntVarArray @solver n 0 1)))

(defn eq- [x y]
  (.makeEquality @solver x y))

(defn neq- [x y]
  (.makeNonEquality @solver x y))

(defn gt- [x y]
  (.makeGreater @solver x y))

(defn sum- 
  ([x y]
    (.makeSum @solver x y))
  ([coll]
    (.makeSum @solver (into-array coll))))

(defn count- [coll v max-c]
  (as-> (map #(.var (eq- % v)) coll) $
    (sum- $)
    (eq- $ max-c)))

(defn nvcount- [coll v max-c]
  (as-> (map #(.var (neq- % v)) coll) $
    (sum- $)
    (eq- $ max-c)))

(defn nmcount- [coll v max-c]
  (as-> (map #(.var (eq- % v)) coll) $
    (sum- $)
    (neq- $ max-c)))

(defn ncount- [coll v max-c]
  (as-> (map #(.var (neq- % v)) coll) $
    (sum- $)
    (neq- $ max-c)))

(defn or- 
  ([c1 c2] (or- [c1 c2]))
  ([cs]
    (as-> cs $
      (map #(.var %) $)
      (into-array $)
      (.makeMax @solver $)
      (.makeEquality @solver $ 1))))

(defn and- 
  ([c1 c2] (and- [c1 c2]))
  ([cs]
    (as-> cs $
      (map #(.var %) $)
      (into-array $)
      (.makeSum @solver $)
      (.makeEquality @solver $ (count cs)))))

(defn gtoreq- [x y]
  (.makeGreaterOrEqual @solver x y))

(defn uniq- [cs]
  (as-> cs $
    (map #(.var %) $)
    (into-array $)
    (.makeCount @solver $ 1 1)))

(defn imply- [c1 c2]
  (or- (.makeEquality @solver (.var c1) 0)
        (.makeEquality @solver (.var c2) 1)))

(defn nimply- [c1 c2]
  (or- (.makeEquality @solver (.var c1) 0)
        (.makeEquality @solver (.var c2) 0)))

(defn add-constraints [& constraints]
  (doseq [c constraints]
    (.addConstraint @solver c)))

(defn default-strategy [] {:var-strategy Solver/CHOOSE_RANDOM
                           :val-strategy Solver/ASSIGN_MAX_VALUE})

(defn make-builder [xs opts] 
  (.makePhase @solver (-> xs (into-array)) (:var-strategy opts) 
                                           (:val-strategy opts)))

(defn init-solver! []
  (reset! solver (make-solver)))

(defn init-lib! []
  (System/loadLibrary "jniortools"))

; (def X 0)
; (def O 1)
; (def blank 2)
; (def side-map {:x X :o O :b blank})

; (defn init-solver
;   ([] (init-solver "tictactoe_solver"))
;   ([name] (Solver. name)))

; (defn set-board-const [^Solver s val i j]
;   (let [board-label (format "board%d_%d" i j)]
;     (if (= val :x) (.makeIntConst s X board-label) (.makeIntConst s O board-label))))

; (defn state-to-stateor [^Solver s state]
;   (let [next-label "next"
;         state-or (update state :next (fn [next-val] 
;                                         (if (= next-val :x) X O)))]
;     (update state-or :board (fn [board] (-> (map-indexed (fn [i row] (-> (map-indexed (fn [j val] (set-board-const s val i j)) row) (vec))) board) (vec))))))

; (defn gen-new-state [^Solver s]
;   (struct game-state/state
;     (.makeIntVar s X O "next")
;     (->
;       (map 
;         (fn [i]
;           (->
;             (map
;               (fn [j]
;                 (let [board-label (format "yboard%d_%d" i j)]
;                   (.makeIntVar s X blank board-label)))
;               (range 0 3))
;             (vec)))
;         (range 0 3))
;       (vec))))

; (defn T [^Solver s x y]
;   (let [xs (-> (state-to-stateor s x) :board (flatten) (into-array))
;         ys (-> y :board (flatten) (into-array))
;         idx (.makeIntVar s 0 8 "i")
;         placed-pieces (.makeBoolVarArray s 9 "placed-piece")]
;     (-> 
;       (.makeSumEquality s placed-pieces 1)
;       (#(.addConstraint s %)))
;     (->
;       (.MakeIndexOfConstraint s placed-pieces idx 1)
;       (#(.addConstraint s %)))
;     ;; x :board is flattened in this source so i has domain [0-8] instead
;     ;; x :board [i,j] == :b (blank), where unqiue [i,j] on domain [0-2]
;     ; (->
;     ;   xs
;     ;   (#(.MakeElementEquality s % idx blank))
;     ;   (#(.addConstraint s %)))
;     ;; y :board [i,j] == x :next (current player), where unqiue [i,j] on domain [0-2]
;     (->
;       ys
;       (#(.MakeElementEquality s % idx (get side-map (:next x))))
;       (#(.addConstraint s %)))
;     (let [x-flat (-> x :board (flatten) (into-array))]
;       (doseq [i (range 9)]
;         (if (not= (nth x-flat i) :b)
;           (->
;             (nth ys i)
;             (#(.makeEquality s % (get side-map (nth x-flat i))))
;             (#(.addConstraint s %))))))
;     (->
;       (:next y)
;       (#(.makeNonEquality s % (get side-map (:next x))))
;       (#(.addConstraint s %)))
;     {:xboard (vec xs) :idx idx :placed-pieces (vec placed-pieces)}))

; (defn add-constraints [^Solver s state]
;   (let [y (gen-new-state s)
;         vars (into {:y y} (T s state y))]
;     vars))

; (defn solve-state [^Solver s vars]
;   (->
;     (.makePhase s (-> vars :y :board 
;                       (flatten)
;                       (#(concat (:xboard vars) %)) 
;                       (#(concat [(-> vars :y :next)] %))
;                       (#(concat [(:idx vars)] %))
;                       (#(concat (:placed-pieces vars) %))
;                       (into-array)) Solver/CHOOSE_FIRST_UNBOUND Solver/ASSIGN_MIN_VALUE)
;     (#(.newSearch s %)))
;   (let [next-sol? (.nextSolution s)
;         board-sol (if next-sol?
;                     ;; debug
;                     (->
;                       (map 
;                         (fn [r]
;                           (->
;                             (map 
;                               (fn [c]
;                                 (filter (comp #{(.value c)} side-map) (keys side-map)))
;                               r)
;                             (vec)))
;                         (-> vars :y :board))
;                       (vec))
;                     [["No Board Solution Found."]])
;         next-sol (if next-sol? (filter (comp #{(.value (-> vars :y :next))} side-map) (keys side-map)) "No Next Solution Found.")]    
;     (.endSearch s)
;     (struct game-state/state next-sol board-sol)))
