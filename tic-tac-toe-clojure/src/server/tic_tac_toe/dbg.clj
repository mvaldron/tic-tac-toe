(ns tic-tac-toe.dbg
  (:gen-class))

(defmacro dbg [x] 
  `(let [x# ~x] 
    (println "dbg:" '~x "=" x#) 
    x#))
