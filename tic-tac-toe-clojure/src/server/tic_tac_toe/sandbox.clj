(ns tic-tac-toe.sandbox
  (:gen-class)
  (:require [tic-tac-toe.dbg :refer :all]
            [schema.core :as s])
  (:import
    (com.google.protobuf GeneratedMessageV3)
    (com.google.ortools.constraintsolver DecisionBuilder
                                         Constraint
                                         IntVar
                                         Solver)))

(def solver (atom nil))
(def ^:dynamic *X* 5)
(def ^:dynamic *O* 6)
(def ^:dynamic *blank* 7)
(def piece {:x *X* :o *O* :b *blank*})
(def piece-of {*X* :x *O* :o *blank* :b})

(s/defschema StateVar {:next IntVar
                       :board [IntVar]})

(s/defn lines-on-board :- [[s/Int]]
  []
  [[0 1 2]
   [3 4 5]
   [6 7 8]
   [0 3 6]
   [1 4 7]
   [2 5 8]
   [0 4 8]
   [2 4 6]])

(s/defschema StateVal {:next (s/enum :x :o)
               :board [(s/enum :x :b :o)]})

(s/defn make-state-val :- StateVal
  ([board :- (:board StateVal)] (make-state-val board :x))
  ([board :- (:board StateVal)
   next :- (:next StateVal)]
   {:next next
    :board board}))

(s/defn print-state-val
  [v :- StateVal]
  (doseq [row (partition 3 (:board v))]
    (println (clojure.string/join " " row)))
  (println (:next v)))

(s/defn make-solver
  ([] (make-solver "tictactoe_solver"))
  ([name :- s/Str] (Solver. name)))

(defn make-int-vars [n]
  (vec (.makeIntVarArray @solver n *X* *blank*)))

(defn make-bool-vars [n]
  (vec (.makeIntVarArray @solver n 0 1)))

(s/defn make-empty-var :- StateVar 
  []
  {:board (make-int-vars 9)
    :next (.makeIntVar @solver *X* *O*)})

(defn count-constraints []
  (.constraints @solver))

(defn state-vars->int-vars [xs]
  (reduce 
    (fn [coll x]
      (conj (into coll (:board x)) (:next x)))
    []
    xs))

(defn int-vars->state-vars [vs & [isval?]]
  (let [vs (if isval? (map piece-of vs) vs)]
    (for [seg (partition 10 vs)]
      {:board (into [] (take 9 seg))
      :next (last seg)})))

(defn state-var->bool-vars [x]
  {:xboard (reduce 
              (fn [coll xi]
                (conj coll xi))
              []
              x)})

(defn eq- [x y]
  (.makeEquality @solver x y))

(defn neq- [x y]
  (.makeNonEquality @solver x y))

(defn gt- [x y]
  (.makeGreater @solver x y))

(defn sum- 
  ([x y]
    (.makeSum @solver x y))
  ([coll]
    (.makeSum @solver (into-array coll))))

(defn count- [coll v max-c]
  (as-> (map #(.var (eq- % v)) coll) $
    (sum- $)
    (eq- $ max-c)))

(defn nvcount- [coll v max-c]
  (as-> (map #(.var (neq- % v)) coll) $
    (sum- $)
    (eq- $ max-c)))

(defn nmcount- [coll v max-c]
  (as-> (map #(.var (eq- % v)) coll) $
    (sum- $)
    (neq- $ max-c)))

(defn ncount- [coll v max-c]
  (as-> (map #(.var (neq- % v)) coll) $
    (sum- $)
    (neq- $ max-c)))

(defn or- 
  ([c1 c2] (or- [c1 c2]))
  ([cs]
    (as-> cs $
      (map #(.var %) $)
      (into-array $)
      (.makeMax @solver $)
      (.makeEquality @solver $ 1))))

(defn and- 
  ([c1 c2] (and- [c1 c2]))
  ([cs]
    (as-> cs $
      (map #(.var %) $)
      (into-array $)
      (.makeSum @solver $)
      (.makeEquality @solver $ (count cs)))))

(defn gtoreq- [x y]
  (.makeGreaterOrEqual @solver x y))

(defn uniq- [cs]
  (as-> cs $
    (map #(.var %) $)
    (into-array $)
    (.makeCount @solver $ 1 1)))

(defn imply- [c1 c2]
  (or- (.makeEquality @solver (.var c1) 0)
       (.makeEquality @solver (.var c2) 1)))

(defn nimply- [c1 c2]
  (or- (.makeEquality @solver (.var c1) 0)
       (.makeEquality @solver (.var c2) 0)))

(s/defschema ConstraintType s/Any)

(s/defn Set :- [ConstraintType] 
  [x :- StateVar
   v :- StateVal]
  (let [pairs (into []
                (map vector (conj (:board x) (:next x))
                            (map piece (conj (:board v) (:next v)))))]
    (for [[var val] pairs]
      (eq- var val))))

(s/defn Move :- [ConstraintType]
  [x :- StateVar
   y :- StateVar]
  (let [xys (map vector (:board x) (:board y))  
        c1 (uniq- (for [[xi yi] xys]
                    (and- (eq- xi (piece :b))
                          (eq- yi (:next x)))))
        c2 (uniq- (for [[xi yi] xys]
                    (neq- xi yi)))
        c3 (neq- (:next x) (:next y))]
    [c1 c2 c3]))

(s/defn Safe :- [ConstraintType]
  [x :- StateVar
   player :- s/Keyword
   opponent :- s/Keyword]
  (let [line-vars (vec (for [line (lines-on-board)]
                          (vec (map #(nth (:board x) %) line))))
        c (and- (for [line-var line-vars]
                    (or-
                      (and- 
                        (count- line-var (piece player) 2) 
                        (count- line-var (piece opponent) 0))
                      (imply- 
                        (count- line-var (piece opponent) 2)
                        (or- (map #(eq- % (piece player)) line-var))))))]
    [c]))

(s/defn Win :- [ConstraintType]
  [x :- StateVar
   y :- StateVar
   player :- s/Keyword
   opponent :- s/Keyword]
  (let [xys (map vector (:board x) (:board y))
        line-vars (vec (for [line (lines-on-board)]
                          (vec (map #(nth (:board y) %) line))))
        c1 (and- (for [[xi yi] xys]
                    (and-
                      (imply-
                        (neq- xi (piece :b))
                        (eq- xi yi))
                      (imply-
                        (eq- xi (piece :b))
                        (or- (eq- yi (piece player)) (eq- yi (piece opponent)))))))
        c2 (uniq- (for [line-var line-vars]
                    (count- line-var (piece player) (count line-var))))
        c3 (and- (for [line-var line-vars]
                    (nmcount- line-var (piece opponent) (count line-var))))]
    [c1 c2 c3]))

(defn add-constraints [& constraints]
  (doseq [c constraints]
    (.addConstraint @solver c)))

(defn default-strategy [] {:var-strategy Solver/CHOOSE_RANDOM
                           :val-strategy Solver/ASSIGN_MAX_VALUE})

(defn make-builder [xs opts] 
  (.makePhase @solver (-> xs (into-array)) (:var-strategy opts) 
                                           (:val-strategy opts)))

(defn solve1
  ([prob] (solve1 prob (default-strategy)))
  ([{xs :int-vars
     cs :constraints} opts]
    (let [builder (make-builder xs opts)]
      (apply add-constraints cs)
      (.newSearch @solver builder)
      (let [sol (if (.nextSolution @solver)
                  (into [] (map #(.value %) xs))
                  nil)]
        (.endSearch @solver)
        sol))))

(defn Solve1 
  ([prob] (Solve1 prob (default-strategy)))
  ([{xs :state-vars
     cs :constraints} opts]
    (let [vs (state-vars->int-vars xs)
          sol (solve1 {:int-vars vs
                       :constraints cs} opts)]
      (int-vars->state-vars sol :is-value))))

(defn init-solver! []
  (reset! solver (make-solver)))

(defn init-lib! []
  (System/loadLibrary "jniortools"))
