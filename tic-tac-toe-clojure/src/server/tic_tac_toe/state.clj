(ns tic-tac-toe.state
  (:gen-class)
  (:require [schema.core :as s])
  (:import
    (com.google.protobuf GeneratedMessageV3)
    (com.google.ortools.constraintsolver IntVar)))

(s/defschema StateVal {:next (s/enum :x :o)
                       :board [(s/enum :x :b :o)]})

(s/defschema StateVar {:next IntVar
                       :board [IntVar]})

(s/defn make-state-val :- StateVal
  ([board :- (:board StateVal)] (make-state-val board :x))
  ([board :- (:board StateVal)
    next :- (:next StateVal)]
    {:next next
     :board board}))

(s/defn state-val->str-vec 
  [board :- (:board StateVal)]
  (vec 
    (for [row (partition 3 board)]
      (clojure.string/join " " row))))

(s/defn print-state-val
  [v :- StateVal]
  (doseq [row (state-val->str-vec (:board v))]
    (println row))
  (println (:next v)))

(s/defn state-val->hiccup
  [v :- StateVal]
  (doseq [row (state-val->str-vec (:board v))]
    (println row))
  (println (:next v)))
