(ns tic-tac-toe.core
    (:gen-class)
    (:require [tic-tac-toe.state :refer :all]
              [tic-tac-toe.solver :as solver]
              [tic-tac-toe.ai :as ai]
              [clojure.pprint :as pp]
              [ring.adapter.jetty :as jetty]
              [ring.middleware.reload :refer [wrap-reload]]
              [ring.middleware.refresh :refer [wrap-refresh]]
              [ring.util.response :refer [response]]
              [compojure.core :as compojure]
              [compojure.route :as route]
              [hiccup.core :as h]))

(defn -main []
  (solver/init-lib!)
  (solver/init-solver!)
  (let [solver (fn [state-val ai-side player-side]
                        (let [x (ai/make-empty-var)
                              y (ai/make-empty-var)
                              yp (ai/make-empty-var)
                              c1 (ai/Set x state-val)
                              c2 (ai/Move x y)
                              c3 (ai/Safe y ai-side player-side)
                              c4 (ai/Win y yp ai-side player-side)
                              ;c4 (Winning y ai-side player-side)
                              ;c5 (Win y ai-side player-side)
                              [sx sy syp] (ai/Solve1 {:state-vars [x y yp]
                                               :constraints (concat c1 c2 c3 c4)})] ;c4 c5)})]
                          (println sy)
                          sy))
              player :x
              ai :o]
    (loop [pre-state nil
            curr-state {:board [:b :b :b 
                                :b :b :b
                                :b :b :b]
                        :next :x}]
      (if (nil? curr-state) 
        pre-state
        (do
          (print-state-val curr-state)
          (if (= (:next curr-state) player)
            (let [ppos (as-> (do (print "Choose piece position (1-9): ") (flush) (read-line)) $
                          (Integer/parseInt $)
                          (- $ 1))]
              (recur curr-state (update (assoc curr-state :next ai) :board #(assoc % ppos player))))
            (recur curr-state (solver curr-state ai player))))))))

; (def title "Google Ortools: Tic Tac Toe")
; (def solver-name "ttt_ai")
; (def exit-key "q")

; (defn print-board [board] 
;   (-> 
;     (map 
;       (fn [r] 
;         (-> 
;           (map (fn [c]
;                   [:td
;                     (cond 
;                       (= c :b) " "
;                       :else c)]) r) 
;           (#(into [:tr] %)))) 
;       board)
;     (#(into [:table] %))))

; (defn run-game [ttt-ai state chosen-side]
;   (let [player-side (if (= chosen-side "x") :x :o)
;         ai-side (if (= chosen-side "x") :o :x)]
;     ))

; (defn test-game [req ttt-ai state]
;   (let [vars (ai/add-constraints ttt-ai state)
;         new-state (ai/solve-state ttt-ai vars)]
;     (h/html
;       [:html
;         [:head
;           [:title title " - Debug"]
;           [:link {:rel "stylesheet"
;                   :href "/static/css/style.css"}]]
;         [:body
;           [:h1 "Output"]
;           [:h3 "Next Player"]
;           [:p (:next new-state)]
;           [:h3 "Board"]
;           (print-board (:board new-state))]])))

(def handler
  (compojure/routes
    (compojure/GET "/" req (h/html [:html 
                                      [:head
                                        [:title "Test - Debug"]
                                        [:link {:rel "stylesheet"
                                                :href "/static/css/style.css"}]]
                                      [:body 
                                        [:h1 "Test Header"]
                                        [:p "Test Content hello there!"]]]))
    (route/resources "/static" {:root "public"})
    (route/not-found (response "Not found"))))

(defn run-server []
  (do
    ; (solver/init-lib!)
    ; (solver/init-solver!)
    (jetty/run-jetty (-> #'handler
                         (wrap-reload)
                         (wrap-refresh ["src" "resources"])) {:port 3000})))
  ;(run-game (ai/init-solver) (init-state) (pick-side)))
