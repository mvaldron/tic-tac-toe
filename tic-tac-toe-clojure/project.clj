(defproject tic-tac-toe "0.1.0-SNAPSHOT"
  :dependencies [;; server-side
                 [org.clojure/clojure "1.8.0"]
                 [ring "1.6.3"]
                 [ring-refresh "0.1.1"]
                 [compojure "1.6.1"]
                 [hiccup "1.0.5"]
                 [io.aviso/pretty "0.1.34"]
                 [prismatic/schema "1.1.9"]
                 
                 ;; client-side
                 [org.clojure/clojurescript "1.9.946"]]
  :profiles {:dev {:plugins [[com.jakemccrary/lein-test-refresh "0.23.0"]]}}
  :plugins [[lein-cljsbuild "1.1.7"]
            [venantius/ultra "0.5.2"]
            [io.aviso/pretty "0.1.34"]
            [lein-jupyter "0.1.16"]]
  :jvm-opts ["-Djava.library.path=./lib"]
  :resource-paths ["resources" "./lib/com.google.ortools.jar" "./lib/protobuf.jar"]
  :native-path "./lib"
  :aot [tic-tac-toe.core tic-tac-toe.solver tic-tac-toe.ai tic-tac-toe.dbg tic-tac-toe.state tic-tac-toe.sandbox]
  :jar-name "tic-tac-toe.jar"
  :source-paths ["src/server"]
  :main tic-tac-toe.core
  :cljs-build { :builds [{:id "dev" 
                          :source-paths ["src/client"] 
                          :compiler {:main tic-tac-toe-game.core 
                                    :output-to "resources/public/js/main.js"
                                    :output-dir "resources/public/js/out"
                                    :asset-path "/static/js/out"
                                    :source-map true}}
                         {:id "prod"
                          :source-paths ["src/client"] 
                          :compiler {:main tic-tac-toe-game.core
                                    :output-to "resources/public/js/main.js"
                                    :optimizations :advanced}}
                        ]})
